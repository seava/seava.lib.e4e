/**
 * Copyright: Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
seava.lib.e4e.js.base.WorkflowFormFactory = {

	createStartForm : function(processDefinitionId) {

		var succesFn = function(response, options) {
			var w = new seava.lib.e4e.js.base.WorkflowFormWithHtmlWindow({
				html : response.responseText,
				_wfConfig_ : {
					type : "startform",
					processDefinitionId : processDefinitionId
				}
			});
			w.show();
		};

		Ext.Ajax.request({
			url : Main.wfProcessDefinitionAPI(processDefinitionId).form,
			method : "GET",
			success : succesFn,
			failure : function() {
				alert('error');
			},
			scope : this
		});

	},

	createTaskForm : function(taskId) {

		var succesFn = function(response, options) {

			var w = new seava.lib.e4e.js.base.WorkflowFormWithHtmlWindow({
				html : response.responseText,
				_wfConfig_ : {
					type : "taskform",
					taskId : taskId
				}
			});
			w.show();
		};

		Ext.Ajax.request({
			url : Main.wfTaskAPI(taskId).form,
			method : "GET",
			success : succesFn,
			failure : function() {
				alert('error');
			},
			scope : this
		});
	}
};
