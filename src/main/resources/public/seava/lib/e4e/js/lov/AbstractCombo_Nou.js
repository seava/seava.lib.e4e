/**
 * Copyright: Nan21 Electronics SRL. All rights reserved. Use is subject to
 * license terms.
 */
Ext.define("seava.lib.e4e.js.lov.AbstractCombo", {

	extend : "Ext.form.field.ComboBox",
	alias : "widget.xcombo",

	// Properties

	/**
	 * 
	 * Link to the dc-view fields to filter the records in this combo.
	 * 
	 * Example: ,filterFieldMapping: [{lovField:"...lovFieldName", dsField:
	 * "...dsFieldName"} ]
	 * 
	 * Or: ,filterFieldMapping: [{lovField:"...lovFieldName", value: "...static
	 * value"} ]
	 */
	filterFieldMapping : null,

	/**
	 * Specify what values should this combo return to the dc-record.
	 * 
	 * @type Array
	 */
	retFieldMapping : null,

	/**
	 * Data-control view type this field belongs to. Injected by the
	 * corresponding view builder.
	 */
	_dcView_ : null,

	/**
	 * Data model signature - record constructor.
	 * 
	 * @type Ext.data.Model
	 */
	recordModel : null,

	recordModelFqn : null,
	/**
	 * Parameters model signature - record constructor.
	 * 
	 * @type Ext.data.Model
	 */
	paramModel : null,
	/**
	 * Parameters model instance
	 */
	params : null,

	// defaults
	triggerAction : "query",
	// matchFieldWidth : false,
	pageSize : 20,
	autoSelect : true,
	minChars : 0,
	queryMode : "remote",

	initComponent : function() {

		this._createStore_();

		if (!Ext.isEmpty(this.paramModel)) {
			this.params = Ext.create(this.paramModel, {});
		}
		if (this.retFieldMapping == null) {
			this.retFieldMapping = [];
		}
		if (this.dataIndex) {
			this.retFieldMapping[this.retFieldMapping.length] = {
				lovField : this.displayField,
				dsField : this.dataIndex
			};
		} else if (this.paramIndex) {
			this.retFieldMapping[this.retFieldMapping.length] = {
				lovField : this.displayField,
				dsParam : this.paramIndex
			};
		}
		// this.on("select", this.assertValueMy, this);

		// this.on("focus", this._onFocus_, this);
		this.callParent(arguments);
	},

	_createStore_ : function() {
		if (this._dataProviderName_ == null) {
			if (Ext.isFunction(this.recordModel)) {
				this.recordModelFqn = this.recordModel.$className;
			} else {
				this.recordModel = Ext.ClassManager.get(this.recordModel)
				this.recordModelFqn = this.recordModel.$className;
			}
			this._dataProviderName_ = this.recordModel.ALIAS;
		}
		this.store = Ext.create("Ext.data.Store", {
			model : this.recordModel,
			remoteSort : true,
			autoLoad : false,
			autoSync : false,
			clearOnPageLoad : true,
			pageSize : this.pageSize,
			proxy : {
				type : 'ajax',
				api : Main.dsAPI(this._dataProviderName_, "json"),
				model : this.recordModel,
				extraParams : {
					params : {}
				},
				actionMethods : {
					create : 'POST',
					read : 'POST',
					update : 'POST',
					destroy : 'POST'
				},
				reader : {
					type : 'json',
					rootProperty : 'data',
					idProperty : 'id',
					totalProperty : 'totalCount',
					messageProperty : 'message'
				},
				listeners : {
					"exception" : {
						fn : this.proxyException,
						scope : this
					}
				},
				startParam : Main.requestParam.START,
				limitParam : Main.requestParam.SIZE,
				sortParam : Main.requestParam.SORT,
				directionParam : Main.requestParam.SENSE
			}
		});
	}

});