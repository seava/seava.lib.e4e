/* ==================== general javascript overrides ======================== */

// string.endsWith 
if (!String.prototype.endsWith) {
	String.prototype.endsWith = function(suffix) {
		return this.indexOf(suffix, this.length - suffix.length) !== -1;
	};
}

// string.toFirstUpper

if (!String.prototype.toFirstUpper) {
	String.prototype.toFirstUpper = function() {
		return this.substring(0, 1).toUpperCase()
				+ this.substring(1, this.length);
	};
}

// string.trim

if (!String.prototype.trim) {
	String.prototype.trim = function() {
		return this.replace(/^\s+|\s+$/g, "");
	}
}

if (!String.prototype.ltrim) {
	String.prototype.ltrim = function() {
		return this.replace(/^\s+/, "");
	}
}

if (!String.prototype.rtrim) {
	String.prototype.rtrim = function() {
		return this.replace(/\s+$/, "");
	}
}

if (!String.prototype.replaceAll) {
	String.prototype.replaceAll = function(find, replace) {
		return this.replace(new RegExp(find.replace(
				/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1"), 'g'), replace);
	}
}

// array.indexOf

if (!Array.prototype.indexOf) {
	Array.prototype.indexOf = function(obj, start) {
		for (var i = (start || 0), j = this.length; i < j; i++) {
			if (this[i] === obj) {
				return i;
			}
		}
		return -1;
	}
}

/* ==================== Extjs overrides ======================== */

Ext.Ajax.timeout = 1000 * 60 * 60;

/**
 * Disable autoloading.
 */
Ext.Loader.setConfig({
	enabled : false
});

/**
 * Change the clientRecordId. The default value `clientId` is in conflict with
 * our clientId field which represents the tenant-id.
 */
Ext.override(Ext.data.Model, {
	clientIdProperty : "__clientRecordId__"
});

Ext.JSON.encodeDate = function(d) {
	return Ext.Date.format(d, '"' + Main.MODEL_DATE_FORMAT + '"');
};

Ext
		.override(
				Ext.data.Model,
				{

					constructor : function(data, session) {

						var me = this, cls = me.self, identifier = cls.identifier, Model = Ext.data.Model, modelIdentifier = Model.identifier, idProperty = me.idField.name, array, id, initializeFn, internalId, len, i, fields;

						me.data = data || (data = {});
						me.session = session || null;
						me.internalId = internalId = modelIdentifier.generate();

						// <debug>
						if (session && !session.isSession) {
							Ext.Error
									.raise('Bad Model constructor argument 2 - "session" is not a Session');
						}
						// </debug>

						if ((array = data) instanceof Array) {
							me.data = data = {};
							fields = me.getFields();
							len = Math.min(fields.length, array.length);
							for (i = 0; i < len; ++i) {
								data[fields[i].name] = array[i];
							}
						}

						if (!(initializeFn = cls.initializeFn)) {
							cls.initializeFn = initializeFn = Model
									.makeInitializeFn(cls);
						}
						if (!initializeFn.$nullFn) {
							cls.initializeFn(me);
						}

						// MyComment - fucking bullshit this all id shit !

						// Must do this after running the initializeFn due to
						// converters on idField
						if (!(me.id = id = data[idProperty]) && id !== 0) {
							if (session) {
								identifier = session.getIdentifier(cls);
								id = identifier.generate();
							} else if (modelIdentifier === identifier) {
								id = internalId;
							} else {
								id = identifier.generate();
							}

							// data[idProperty] =
							me.id = id;
							me.phantom = true;
						}

						if (session) {
							session.add(me);
						}

						if (me.init && Ext.isFunction(me.init)) {
							me.init();
						}
					}

				});

Ext
		.override(
				Ext.data.operation.Operation,
				{

					doProcess : function(resultSet, request, response) {
						var me = this, commitSetOptions = me._commitSetOptions, clientRecords = me
								.getRecords(), clientLen = clientRecords.length, clientIdProperty = clientRecords[0].clientIdProperty, serverRecords = resultSet
								.getRecords(), serverLen = serverRecords ? serverRecords.length
								: 0, clientMap, serverRecord, clientRecord, i;
						if (serverLen && clientIdProperty) {
							clientMap = Ext.Array.toValueMap(clientRecords,
									'id');
							for (i = 0; i < serverLen; ++i) {
								serverRecord = serverRecords[i];
								var _key = serverRecord[clientIdProperty]
								if (!_key) {
									_key = serverRecord["id"]
								}

								clientRecord = clientMap[_key];
								if (clientRecord) {
									delete clientMap[clientRecord.id];
									delete serverRecord[clientIdProperty];
									var oldId = clientRecord.id;
									clientRecord.set(serverRecord,
											commitSetOptions);
									clientRecord.id = oldId;
								} else {
									Ext.log.warn('Ignoring server record: '
											+ Ext.encode(serverRecord));
								}
							}
							for (i in clientMap) {
								clientMap[i].commit();
							}
						} else {
							for (i = 0; i < clientLen; ++i) {
								clientRecord = clientRecords[i];
								if (serverLen === 0
										|| !(serverRecord = serverRecords[i])) {
									clientRecord.commit();
								} else {
									clientRecord.set(serverRecord,
											commitSetOptions);
								}
							}
						}
					}
				});

Ext
		.override(
				Ext.data.writer.Writer,
				{

					// Tibi: E5
					getRecordData : function(record, operation) {
						var me = this, nameProperty = me.getNameProperty(), mapping = nameProperty !== 'name', idField = record.self.idField, key = idField[nameProperty]
								|| idField.name, // setup for idField first
						value = record.id, writeAll = me.getWriteAllFields(), ret, dateFormat, phantom, options, clientIdProperty, fieldsMap, data, field;

						if (idField.serialize) {
							value = idField.serialize(value);
						}

						if (!writeAll && operation
								&& operation.isDestroyOperation) {
							ret = {};
							ret[key] = value;
						} else {
							dateFormat = me.getDateFormat();
							phantom = record.phantom;
							options = (phantom || writeAll) ? me
									.getAllDataOptions() : me
									.getPartialDataOptions();
							// Tibi: E5
							// clientIdProperty = phantom &&
							// me.getClientIdProperty();
							clientIdProperty = me.getClientIdProperty();

							fieldsMap = record.getFieldsMap();

							options.serialize = false; // we must take over
							// this here
							data = record.getData(options);

							// If we are mapping we need to pour data into a new
							// object, otherwise we do
							// our work in-place:
							ret = mapping ? {} : data;

							if (clientIdProperty) { // if (phantom and have
								// clientIdProperty)
								ret[clientIdProperty] = value; // must read
								// data and
								// write ret
								// Tibi: E5
								// delete data[key]; // in case ret === data
								// (must not send "id")
							} else if (!me.getWriteRecordId()) {
								delete data[key];
							}

							for (key in data) {
								value = data[key];

								if (!(field = fieldsMap[key])) {
									// No defined field, so clearly no
									// nameProperty to look up for this field
									// but if we are mapping we need to copy
									// over the value. Also there is no
									// serializer to call in this case.
									if (mapping) {
										ret[key] = value;
									}
								} else {
									// Allow this Writer to take over formatting
									// date values if it has a
									// dateFormat specified. Only check isDate
									// on fields declared as dates
									// for efficiency.
									if (field.isDateField && dateFormat
											&& Ext.isDate(value)) {
										value = Ext.Date.format(value,
												dateFormat);
									} else if (field.serialize) {
										value = field.serialize(value, record);
									}

									if (mapping) {
										key = field[nameProperty] || key;
									}

									ret[key] = value;
								}
							}
						}

						return ret;
					}
				});

/**
 * Override page loading: - if totalCount is set check if next-page doesn't
 * overflow - check previous page isn't negative - helper filter methods
 * 
 */
Ext.override(Ext.data.Store, {

	filterAllNew : function(item) {
		return item.phantom === true;
	},

	getAllNewRecords : function() {
		return this.data.filterBy(this.filterAllNew).items;
	},

	filterUpdated : function(item) {
		return item.dirty === true && item.phantom !== true;
	},

	getModifiedRecords : function() {
		return [].concat(this.getAllNewRecords(), this.getUpdatedRecords());
	},

	rejectChanges : function() {
		this.callParent();
		this.fireEvent('changes_rejected', this);
	}

});

/**
 * Disabled fields do not allow focus, so user cannot copy-paste values from
 * them. This override allows to perform enable/disable as read-only based on
 * the Main.viewConfig.DISABLE_AS_READONLY flag. All the field enable/disabe
 * calls are routed through these dispatcher functions.
 * 
 */

Ext.override(Ext.Img, {
	_enable_ : function() {
	},

	_disable_ : function() {
	},

	_setDisabled_ : function(v) {
	}
});

Ext.override(Ext.form.field.Base, {

	_enable_ : function() {
		if (Main.viewConfig.DISABLE_AS_READONLY === true) {
			if (this.readOnly === true) {
				this.setReadOnly(false);
			}
		} else {
			if (this.disabled === true) {
				this.enable();
			}
		}
	},

	_disable_ : function() {
		if (Main.viewConfig.DISABLE_AS_READONLY === true) {
			if (this.readOnly === false) {
				this.setReadOnly(true);
			}
		} else {
			if (this.disabled === false) {
				this.disable();
			}
		}
	},

	_setDisabled_ : function(v) {
		if (Main.viewConfig.DISABLE_AS_READONLY === true) {
			if (this.readOnly !== v) {
				this.setReadOnly(v);
			}

		} else {
			if (this.disabled !== v) {
				this.setDisabled(v);
			}
		}
	}

});

/**
 * Add my _enable_ / _disable_ handlers
 */
Ext.override(Ext.container.Container, {
	_enable_ : function(args) {
		this.enable(args);
	},

	_disable_ : function(args) {
		this.disable(args);
	},

	_setDisabled_ : function(args) {
		this.setDisabled(args);
	}
});

/**
 * Add my _enable_ / _disable_ handlers
 */
Ext.override(Ext.button.Button, {
	_enable_ : function(args) {
		this.enable(args);
	},

	_disable_ : function(args) {
		this.disable(args);
	},

	_setDisabled_ : function(args) {
		this.setDisabled(args);
	}
});

// Tibi: E5 - overrides and hack to work with ExtJS 5.1

Ext
		.override(
				Ext.form.field.ComboBox,
				{

					doSetValue : function(value, add) {
						var me = this, store = me.getStore(), Model = store
								.getModel(), matchedRecords = [], valueArray = [], key, autoLoadOnValue = me.autoLoadOnValue, isLoaded = store
								.getCount() > 0
								|| store.isLoaded(), pendingLoad = store
								.hasPendingLoad(), unloaded = autoLoadOnValue
								&& !isLoaded && !pendingLoad, forceSelection = me.forceSelection, selModel = me.pickerSelectionModel, displayTplData = me.displayTplData
								|| (me.displayTplData = []), displayIsValue = me.displayField === me.valueField, i, len, record, dataObj, raw;

						if (add && !me.multiSelect) {
							Ext.Error
									.raise('Cannot add values to non muiltiSelect ComboBox');
						}

						if (value != null
								&& !displayIsValue
								&& (pendingLoad || unloaded || !isLoaded || store.isEmptyStore)) {
							if (value.isModel) {
								displayTplData.length = 0;
								displayTplData.push(value.data);
								raw = me.getDisplayValue();
							}
							if (add) {
								me.value = Ext.Array.from(me.value).concat(
										value);
							} else {
								if (value.isModel) {
									value = value.get(me.valueField);
								}
								me.value = value;
							}
							me.setHiddenValue(me.value);

							me.setRawValue(raw || '');
							if (unloaded && store.getProxy().isRemote) {
								store.load();
							}
							return me;
						}

						value = add ? Ext.Array.from(me.value).concat(value)
								: Ext.Array.from(value);

						for (i = 0, len = value.length; i < len; i++) {
							record = value[i];

							if (!record || !record.isModel) {
								record = me.findRecordByValue(key = record);

								if (!record) {
									record = me.valueCollection.find(
											me.valueField, key);
								}
							}

							if (!record) {

								// if (!forceSelection) {
								// Tibi: E5 - when setValue is called create and
								// add the record even if is not in the store
								// list
								// when we have paginated list, the record could
								// be on any page
								// if (!record) {
								dataObj = {};
								dataObj[me.displayField] = value[i];
								if (me.valueField
										&& me.displayField !== me.valueField) {
									dataObj[me.valueField] = value[i];
								}
								record = new Model(dataObj);
								// }
								// }
								// else if (me.valueNotFoundRecord) {
								// record = me.valueNotFoundRecord;
								// }
							}

							if (record) {
								matchedRecords.push(record);
								valueArray.push(record.get(me.valueField));
							}
						}
						me.lastSelection = matchedRecords;

						me.suspendEvent('select');
						me.valueCollection.beginUpdate();
						if (matchedRecords.length) {
							selModel.select(matchedRecords, false);
						} else {
							selModel.deselectAll();
						}
						me.valueCollection.endUpdate();
						me.resumeEvent('select');
						return me;
					}

				});

// keep window inside in his container
Ext.override(Ext.window.Window, {

	initComponent : function() {
		this.callParent();
		// do not allow for window header to move outside of its container
		// if header is not defined, keep the whole window inside of container
		if (this.header) {
			this.constrainHeader = true;
		} else {
			this.constrain = true;
		}
	}
});

Ext.override(Ext.form.field.Text, {

	getRawValue : function() {
		var me = this, v = me.callParent();
		if (v === me.emptyText && me.valueContainsPlaceholder) {
			v = '';
		}
		if (this.caseRestriction && v != '') {
			if (this.caseRestriction == "uppercase") {
				v = v.toUpperCase();
			} else {
				v = v.toLowerCase();
			}
			me.rawValue = v;
		}
		return v;
	}

});

/**
 * With an LOV type of editor do not complete edit on first ENTER, just collapse
 * the picker list view
 * 
 */
Ext.override(Ext.Editor, {

	onSpecialKey : function(field, event) {

		var me = this, key = event.getKey(), complete = me.completeOnEnter
				&& key === event.ENTER, cancel = me.cancelOnEsc
				&& key === event.ESC, task = me.specialKeyTask;

		if (field._isLov_ && field.isExpanded === true) {
			complete = false;
		}

		if (complete || cancel) {
			event.stopEvent();
			if (!task) {
				me.specialKeyTask = task = new Ext.util.DelayedTask();
			}
			var fSpecKey = function() {
				if (complete) {
					me.completeEdit();
					var _col = field.column;
					if (field.triggerBlur) {
						if (_col && _col._dcView_) {
							_col._dcView_.getView().focus();
						}
						field.triggerBlur(event);
					}
				} else {
					me.cancelEdit();
					if (field.triggerBlur) {
						field.triggerBlur(event);
					}
				}
			};
			// Must defer this slightly to prevent exiting edit mode
			// before the field's own
			// key nav can handle the enter key, e.g. selecting an item
			// in a combobox list
			// task.delay(me.specialKeyDelay, complete ? me.completeEdit :
			// me.cancelEdit, me);
			task.delay(me.specialKeyDelay, fSpecKey, me);
			// <debug>
			// Makes unit testing easier
			if (me.specialKeyDelay === 0) {
				task.cancel();
				fSpecKey();
			}
			// </debug>
		}

		me.fireEvent('specialkey', me, field, event);
	}
});

Ext.override(Ext.grid.plugin.CellEditing, {

	/**
	 * Context content { grid : grid, record : record, field :
	 * columnHeader.dataIndex, value : record.get(columnHeader.dataIndex), row :
	 * view.getNode(rowIdx), column : columnHeader, rowIdx : rowIdx, colIdx :
	 * colIdx }
	 * 
	 * @param {}
	 *            context
	 * @return {Boolean}
	 */
	beforeEdit : function(context) {
		if (context.store && context.store.isLoading()) {
			return false;
		}
		if (context.grid && context.grid.beforeEdit) {
			return context.grid.beforeEdit(context);
		}
	},

	_isEditAllowed_ : function(record, column, field, grid) {
		if (field && field.noEdit) {
			return false;
		}
		if (field && field.noUpdate === true && !record.phantom) {
			return false;
		} else if (field && field.noInsert === true && record.phantom) {
			return false;
		} else if (field._enableFn_) {
			var fn = field._enableFn_;
			if (grid != null) {
				return fn.call(grid, grid._controller_, record, column, field);
			} else {
				return fn.call(this, null, record, column, field);
			}
		}
		return true;
	},

	getEditor : function(record, column) {

		var me = this;
		if (me.grid._getCustomCellEditor_) {
			var editor = me.grid._getCustomCellEditor_(record, column);
			if (editor != null) {
				if (!this._isEditAllowed_(record, column, editor.field
						|| editor)) {
					return null;
				}

				if (!(editor instanceof Ext.grid.CellEditor)) {
					editorId = column.id + record.id;
					editor = new Ext.grid.CellEditor({
						floating : true,
						editorId : editorId,
						field : editor
					});
				}
				var editorOwner = me.grid.ownerLockable || me.grid;
				editorOwner.add(editor);
				editor.on({
					scope : me,
					specialkey : me.onSpecialKey,
					complete : me.onEditComplete,
					canceledit : me.cancelEdit
				});

				column.on('removed', me.cancelActiveEdit, me);
				editor.field["_targetRecord_"] = record;

				editor.grid = me.grid;
				// Keep upward pointer correct for each use - editors are shared
				// between locking sides
				editor.editingPlugin = me;

				return editor;
			}
		}

		var editor = this.callParent(arguments);

		var editAllowed = true;
		if (me.grid) {
			editAllowed = this._isEditAllowed_(record, column, editor.field
					|| editor, me.grid);
		} else {
			editAllowed = this._isEditAllowed_(record, column, editor.field
					|| editor, null);
		}

		if (editor && editAllowed === false) {
			return false;
		}

		if (editor.field.caseRestriction) {
			editor.field.fieldStyle = "text-transform:"
					+ editor.field.caseRestriction + ";";
		}

		if (editor.field) {
			if (me.grid && (me.grid._dcViewType_ == "filter-propgrid")) {
				editor.field["_targetRecord_"] = me.grid._controller_.filter;
			} else {
				editor.field["_targetRecord_"] = record;
			}

		}

		return editor;
	},

	showEditor : function(ed, context, value) {
		this.callParent(arguments);
		if (ed.field && Ext.isFunction(ed.field.validate)) {
			ed.field.validate();
		}
	}
});

// known issue in ExtJS 5.1.0
// http://www.sencha.com/forum/showthread.php?294953-Tab-switching-bug
// will be fixed in 5.1.1
// SONE-1229
Ext
		.override(
				Ext.tab.Bar,
				{

					initComponent : function() {
						var me = this, initialLayout = me.initialConfig.layout, initialAlign = initialLayout
								&& initialLayout.align, initialOverflowHandler = initialLayout
								&& initialLayout.overflowHandler, layout;

						if (me.plain) {
							me.addCls(me.baseCls + '-plain');
						}

						me.callParent();

						me.setLayout({
							align : initialAlign
									|| (me.getTabStretchMax() ? 'stretchmax'
											: me._layoutAlign[me.dock]),
							overflowHandler : initialOverflowHandler
									|| 'scroller'
						});

						// We have to use mousedown here as opposed to click
						// event, because
						// Firefox will not fire click in certain cases if
						// mousedown/mouseup
						// happens within btnInnerEl.
						me.on({
							mousedown : me.onClick,
							element : 'el',
							scope : me
						});
					}
				});
