/**
 * Copyright: Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
/**
 * Builder for filter-form views.
 */
Ext.define("seava.lib.e4e.js.dc.view.DcvFilterFormBuilder", {
	extend : "Ext.util.Observable",

	dcv : null,

	addTextField : function(config) {
		config.xtype = "textfield";
		this.applySharedConfig(config);
		return this;
	},

	addTextArea : function(config) {
		config.xtype = "textarea";
		this.applySharedConfig(config);
		return this;
	},

	addCheckbox : function(config) {
		config.xtype = "checkbox";
		this.applySharedConfig(config);
		return this;
	},

	addBooleanField : function(config) {
		Ext.applyIf(config, {
			forceSelection : false
		});

		var yesNoStore = Ext.create('Ext.data.Store', {
			fields : [ "bv", "tv" ],
			data : [ {
				"bv" : true,
				"tv" : Main.translate("msg", "bool_true")
			}, {
				"bv" : false,
				"tv" : Main.translate("msg", "bool_false")
			} ]
		});

		Ext.apply(config, {
			xtype : "combo",
			queryMode : "local",
			valueField : "bv",
			displayField : "tv",
			triggerAction : "all",
			store : yesNoStore
		});
		this.applySharedConfig(config);
		return this;
	},

	addDateField : function(config) {
		config.xtype = "datefield";
		Ext.applyIf(config, {
			_mask_ : Masks.DATE
		});
		config.format = Main[config._mask_];
		this.applySharedConfig(config);
		return this;
	},

	addNumberField : function(config) {
		Ext.apply(config || {}, {
			xtype : "numberfield",
			hideTrigger : true,
			keyNavEnabled : false,
			mouseWheelEnabled : false
		});
		this.applySharedConfig(config);
		return this;
	},

	addLov : function(config) {
		this.applySharedConfig(config);
		return this;
	},

	addCombo : function(config) {
		Ext.applyIf(config, {
			xtype : "combo"
		});
		this.applySharedConfig(config);
		return this;
	},

	addPanel : function(config) {
		Ext.applyIf(config, this.dcv.defaults);
		var d = config.defaults || {};
		var cd = config.childrenDefaults || {};
		Ext.apply(d, cd)
		Ext.applyIf(config, {
			defaults : d,
			xtype : "container",
			id : Ext.id()
		});
		if (config._hasTitle_ === true) {
			config.title = this.dcv.translate(config.name + "__ttl");
		}
		this.dcv._elems_.add(config.name, config);
		return this;
	},

	addFieldContainer : function(config) {
		Ext.applyIf(config, {
			xtype : 'fieldcontainer',
			layout : 'hbox',
			combineErrors : true,
			defaults : {
				flex : 1,
				hideLabel : true
			}
		});
		this.dcv._elems_.add(config.name, config);
		return this;
	},

	addButton : function(config) {
		Ext.applyIf(config, {
			id : Ext.id(),
			xtype : "button"
		});
		this.dcv._elems_.add(config.name, config);
		return this;
	},

	addChildrenTo : function(c, list) {
		var items = this.dcv._elems_.get(c)["items"] || [];
		for (var i = 0, len = list.length; i < len; i++) {
			items[items.length] = this.dcv._elems_.get(list[i]);
		}
		this.dcv._elems_.get(c)["items"] = items;
		return this;
	},

	add : function(config) {
		this.applySharedConfig(config);
		return this;
	},

	merge : function(name, config) {
		Ext.applyIf(this.dcv._elems_.get(name), config);
		return this;
	},

	change : function(name, config) {
		Ext.apply(this.dcv._elems_.get(name), config);
		return this;
	},

	remove : function(name) {
		this.dcv._elems_.remove(name);
		return this;
	},

	// ==============================================
	// ==============================================

	applySharedConfig : function(config) {
		Ext.applyIf(config, {
			id : Ext.id(),
			selectOnFocus : true,
			_dcView_ : this.dcv,
			shrinkWrap : 0
		});
		if (config.maxLength) {
			config.enforceMaxLength = true;
		}
		if (config.caseRestriction) {
			config.fieldStyle = "text-transform:" + config.caseRestriction
					+ ";";
		}
		if (config.allowBlank === false && config.noLabel !== true) {
			config.labelSeparator = "*";
		}
		if (config.noEdit === true) {
			config.readOnly = true;
		}
		this.dcv._elems_.add(config.name, config);
	}
});
