/**
 * Copyright: Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.lib.e4e.js.dc.command.DcReloadRecCommand", {
	extend : "seava.lib.e4e.js.dc.command.AbstractDcAsyncCommand",

	dcApiMethod : seava.lib.e4e.js.dc.DcActionsFactory.RELOAD_REC,

	onExecute : function(options) {
		var dc = this.dc;
		Ext.Ajax.request({
			url : dc.store.proxy.api.load,
			params : {
				data : Ext.encode({
					id : dc.record.data.id
				})
			},
			success : this.onReload,
			scope : this
		});
	},

	onReload : function(response, opts) {
		var dc = this.dc;
		var rs = dc.store.proxy.reader.read(response);
		var r = dc.record;
		var nr = rs.records[0];
		var shouldCommit = !r.dirty;
		r.beginEdit();
		for ( var p in dc.record.data) {
			dc.record.set(p, nr.data[p]);
		}
		r.endEdit();
		if (shouldCommit) {
			r.commit();
		}
		dc.fireEvent('recordReload', {
			dc : dc,
			record : r
		});
	},

	isActionAllowed : function() {
		if (seava.lib.e4e.js.dc.DcActionsStateManager.isReloadRecDisabled(this.dc)) {
			this.dc.info(Main.msg.DC_RELOAD_RECORD_NOT_ALLOWED, "msg");
			return false;
		}
		return true;
	}

});
