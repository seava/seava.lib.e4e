/**
 * Copyright: Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.lib.e4e.js.dc.command.DcNextRecCommand", {
	extend : "seava.lib.e4e.js.dc.command.AbstractDcSyncCommand",

	dcApiMethod : seava.lib.e4e.js.dc.DcActionsFactory.NEXT_REC,

	onExecute : function(options) {
		var dc = this.dc;
		if (dc.selectedRecords.length <= 1) {
			var crtIdx = dc.store.indexOf(dc.record);
			if (++crtIdx >= dc.store.getCount()) {
				this.dc.info(Main.msg.AT_LAST_RECORD, "msg");
			} else {
				dc.setRecord(dc.store.getAt(crtIdx), true);
			}
		} else {
			var crtIdx = dc.selectedRecords.indexOf(dc.record);
			if (++crtIdx >= dc.selectedRecords.length) {
				this.dc.info(Main.msg.AT_LAST_RECORD, "msg");
			} else {
				dc.setRecord(dc.selectedRecords[crtIdx], false);
			}
		}
	},

	isActionAllowed : function() {
		if (seava.lib.e4e.js.dc.DcActionsStateManager.isNextRecDisabled(this.dc)) {
			this.dc.warning(Main.msg.DC_RECORD_CHANGE_NOT_ALLOWED, "msg");
			return false;
		}
		return true;
	}
});
