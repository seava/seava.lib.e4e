/**
 * Copyright: Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.lib.e4e.js.dc.command.DcEditOutCommand", {
	extend : "seava.lib.e4e.js.dc.command.AbstractDcSyncCommand",

	dcApiMethod : seava.lib.e4e.js.dc.DcActionsFactory.EDIT_OUT,

	onExecute : function(options) {
		var dc = this.dc;
		if (dc.trackEditMode) {
			dc.isEditMode = false;
		}
		dc.fireEvent("onEditOut", dc, options);
	},

	isActionAllowed : function() {
		if (seava.lib.e4e.js.dc.DcActionsStateManager.isEditOutDisabled(this.dc)) {
			this.dc.warning(Main.msg.DC_EDIT_OUT_NOT_ALLOWED, "msg");
			return false;
		}
		return true;
	}
});
