/**
 * Copyright: Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.lib.e4e.js.dc.command.DcQueryCommand", {
	extend : "seava.lib.e4e.js.dc.command.AbstractDcAsyncCommand",

	dcApiMethod : seava.lib.e4e.js.dc.DcActionsFactory.RUN_QUERY,

	onExecute : function(options) {
		var dc = this.dc;
		var _p = dc.buildRequestParamsForQuery();
		Ext.apply(dc.store.proxy.extraParams, _p);
		Main.working();
		dc.store.load({
			callback : function(records, operation, success) {

				this.onAjaxResult({
					records : records,
					response : operation.response,
					operation : operation,
					options : options,
					success : success
				});
			},
			scope : this,
			options : options,
			page : 1
		});
		// the paging doesn't get refreshed as the store doesn't update its
		// currentPage. Force it here, hopefully will be fixed
		// TODO: check me on Extjs upgrade!
		this.dc.store.currentPage = 1;
	},

	isActionAllowed : function() {
		if (seava.lib.e4e.js.dc.DcActionsStateManager.isQueryDisabled(this.dc)) {
			this.dc.warning(Main.msg.DC_QUERY_NOT_ALLOWED, "msg");
			return false;
		}
		var res = true;
		var dc = this.dc;
		if (!dc.filter.isValid()) {
			this.dc.error(Main.msg.INVALID_FILTER, "msg");
			res = false;
		}
		return res;
	}
});
