/**
 * Copyright: Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.lib.e4e.js.dc.command.DcEditInCommand", {
	extend : "seava.lib.e4e.js.dc.command.AbstractDcSyncCommand",

	dcApiMethod : seava.lib.e4e.js.dc.DcActionsFactory.EDIT_IN,

	onExecute : function(options) {
		var dc = this.dc;
		if (dc.trackEditMode) {
			dc.isEditMode = true;
		}
		dc.fireEvent("onEditIn", dc, options);
	}
});
