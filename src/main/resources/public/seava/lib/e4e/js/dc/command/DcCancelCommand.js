/**
 * Copyright: Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.lib.e4e.js.dc.command.DcCancelCommand", {
	extend : "seava.lib.e4e.js.dc.command.AbstractDcSyncCommand",

	dcApiMethod : seava.lib.e4e.js.dc.DcActionsFactory.CANCEL,
	
	onExecute : function(options) {
		var dc = this.dc;
		if (dc.store.getCount() == 0) {
			this.discardChanges();
		} else {
			this.discardChildrenChanges();
			this.discardChanges();
		}
		if (dc.record == null) {
			dc.doDefaultSelection();
		}
	},

	discardChanges : function() {

		var dc = this.dc;
		var s = dc.store;

		if (dc.record && dc.record.phantom) {
			/*
			 * workaround to avoid the dirty check in AbstractDc.setRecord
			 */
			var cr = dc.record;
			cr.phantom = false;
			cr.dirty = false;
			dc.setRecord(null, true);
			cr.phantom = true;
			cr.dirty = true;
		}
		s.rejectChanges();
	},

	discardChildrenChanges : function() {
		var dc = this.dc;
		var dirty = false, l = dc.children.length;
		for ( var i = 0; i < l; i++) {
			if (dc.children[i].isDirty()) {
				dc.children[i].doCancel();
			}
		}
	}

});
