/**
 * Copyright: Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.lib.e4e.js.dc.command.DcDeleteCommand", {
	extend : "seava.lib.e4e.js.dc.command.AbstractDcAsyncCommand",

	dcApiMethod : seava.lib.e4e.js.dc.DcActionsFactory.DELETE,

	constructor : function(config) {
		this.callParent(arguments);
		this.confirmByUser = true;
		this.confirmMessageTitle = Main.translate("msg", "dc_confirm_action");
		this.confirmMessageBody = Main.translate("msg",
				"dc_confirm_delete_selection");
	},

	onExecute : function(options) {
		var dc = this.dc;
		dc.store.remove(dc.getSelectedRecords());
		if (!dc.multiEdit) {
			dc.store.sync({
				callback : function(batch, options) {
					this.onAjaxResult({
						batch : batch,
						options : options,
						success : !batch.exception
					});
				},
				scope : this,
				options : options
			});
		} 
		else {
			dc.doDefaultSelection();
		}
	},

	onAjaxSuccess : function(ajaxResult) {
		this.callParent(arguments);
		this.dc.doDefaultSelection();
		this.dc.fireEvent("afterDoCommitSuccess", this.dc,
				ajaxResult.options.options);
	},

	isActionAllowed : function() {
		if (seava.lib.e4e.js.dc.DcActionsStateManager.isDeleteDisabled(this.dc)) {
			this.dc.warning(Main.msg.DC_DELETE_NOT_ALLOWED, "msg");
			return false;
		}
		return true;
	}

});