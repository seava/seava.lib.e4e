/**
 * Copyright: Nan21 Electronics SRL. All rights reserved. Use is subject to
 * license terms.
 */
Ext.define("seava.lib.e4e.js.dc.command.DcClearQueryCommand", {
	extend : "seava.lib.e4e.js.dc.command.AbstractDcSyncCommand",

	dcApiMethod : seava.lib.e4e.js.dc.DcActionsFactory.CLEAR_QUERY,

	onExecute : function(options) {

		var dc = this.dc;
		dc.advancedFilter = null;
		var f = dc.filter;

		var fcf = {};
		var fcp = {};

		if (dc.flowContext) {
			fcf = dc.flowContext.filter;
			fcp = dc.flowContext.params;
		}

		// reset fields
		for ( var k in dc.filter.data) {
			if (fcf[k] === undefined) {
				dc.setFilterValue(k, null, false, "clearQuery");
			} else {
				dc.setFilterValue(k, fcf[k], false, "clearQuery");
			}
		}

		// reset params used for filter
		var p = dc.params;
		if (p) {

			for ( var f in p.fields) {
				if (f.forFilter === true) {
					var k = p.name;
					if (fcp[k] === undefined) {
						dc.setParamValue(k, null, false, "clearQuery");
					} else {
						dc.setParamValue(k, fcp[k], false, "clearQuery");
					}
				}
			}
		}

		// apply context filter
		if (dc.dcContext) {
			dc.dcContext._updateChildFilter_();
		}

		dc.store.loadData([], false);
	},

	isActionAllowed : function() {
		if (seava.lib.e4e.js.dc.DcActionsStateManager
				.isClearQueryDisabled(this.dc)) {
			this.dc.warning(Main.msg.DIRTY_DATA_FOUND, "msg");
			return false;
		}
	}

});