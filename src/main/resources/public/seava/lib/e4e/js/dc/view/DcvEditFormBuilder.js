/**
 * Copyright: Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
/**
 * Builder for edit-form views.
 */
Ext.define("seava.lib.e4e.js.dc.view.DcvEditFormBuilder", {
	extend : "Ext.util.Observable",

	dcv : null,

	addTextField : function(config) {
		config.xtype = "textfield";
		this.applySharedConfig(config);
		return this;
	},

	addTextArea : function(config) {
		config.xtype = "textarea";
		Ext.applyIf(config, {
			selectOnFocus : false
		});
		this.applySharedConfig(config);
		return this;
	},

	addBooleanField : function(config) {
		return this.addCheckbox(config);
	},

	addCheckbox : function(config) {
		config.xtype = "checkbox";
		config.value = false;
		config.checked = false;
		config.uncheckedValue = false;
		this.applySharedConfig(config);
		return this;
	},

	addDateField : function(config) {
		config.xtype = "datefield";
		Ext.applyIf(config, {
			_mask_ : Masks.DATE,
			altFormats : Main.ALT_FORMATS
		});
		config.format = Main[config._mask_];
		this.applySharedConfig(config);
		return this;
	},

	addDateTimeField : function(config) {
		config.xtype = "datefield";
		Ext.applyIf(config, {
			_mask_ : Masks.DATETIME,
			altFormats : Main.ALT_FORMATS
		});
		config.format = Main[config._mask_];
		this.applySharedConfig(config);
		return this;
	},

	addNumberField : function(config) {
		Ext.apply(config || {}, {
			xtype : "numberfield",
			hideTrigger : true,
			keyNavEnabled : false,
			mouseWheelEnabled : false
		});
		if (config.decimals) {
			config.decimalPrecision = config.decimals;
		}
		config.fieldStyle = (config.fieldStyle) ? config.fieldStyle
				+ ";text-align:right;" : "text-align:right;";
		this.applySharedConfig(config);
		return this;
	},

	addLov : function(config) {
		Ext.applyIf(config, {
		// forceSelection : true
		});
		this.applySharedConfig(config);
		return this;
	},

	addCombo : function(config) {
		Ext.applyIf(config, {
			xtype : "combo"
		});
		this.applySharedConfig(config);
		return this;
	},

	addHiddenField : function(config) {
		config.xtype = "hiddenfield";
		Ext.applyIf(config, {
			maxLength : 20
		});
		this.applySharedConfig(config);
		return this;
	},

	addImage : function(config) {
		config.xtype = "image";
		Ext.applyIf(config, {
			border : true,
			style : {
				backgroundColor : "#fff",
				border : "1px solid #777"
			}
		});
		if (this.dcv._images_ == null) {
			var imgs = this.dcv._images_ = [];
			imgs[imgs.length] = config.name;
		}
		this.applySharedConfig(config);
		return this;
	},

	addPanel : function(config) {
		Ext.applyIf(config, this.dcv.defaults);
		var d = config.defaults || {};
		var cd = config.childrenDefaults || {};
		Ext.apply(d, cd)
		Ext.applyIf(config, {
			defaults : d,
			xtype : "container",
			id : Ext.id()
		});
		if (config._hasTitle_ === true) {
			config.title = this.dcv.translate(config.name + "__ttl");
		}
		this.dcv._elems_.add(config.name, config);
		return this;
	},

	addButton : function(config) {
		Ext.applyIf(config, {
			id : Ext.id(),
			xtype : "button"
		});
		this.dcv._elems_.add(config.name, config);
		return this;
	},

	addChildrenTo : function(c, list) {
		var items = this.dcv._elems_.get(c)["items"] || [];
		for (var i = 0, len = list.length; i < len; i++) {
			items[items.length] = this.dcv._elems_.get(list[i]);
		}
		this.dcv._elems_.get(c)["items"] = items;
		return this;
	},

	add : function(config) {
		this.applySharedConfig(config);
		return this;
	},

	merge : function(name, config) {
		Ext.applyIf(this.dcv._elems_.get(name), config);
		return this;
	},

	change : function(name, config) {
		Ext.apply(this.dcv._elems_.get(name), config);
		return this;
	},

	remove : function(name) {
		this.dcv._elems_.remove(name);
		return this;
	},

	// private

	applySharedConfig : function(config) {
		Ext.applyIf(config, {
			id : Ext.id(),
			// itemId : config.name,
			selectOnFocus : true,
			_dcView_ : this.dcv,
			shrinkWrap : 0
		});
		if (config.maxLength) {
			config.enforceMaxLength = true;
		}
		if (config.caseRestriction) {
			config.fieldStyle = "text-transform:" + config.caseRestriction
					+ ";";
		}
		if (config.allowBlank === false && config.noLabel !== true) {
			config.labelSeparator = "*";
		}
		this.dcv._elems_.add(config.name, config);
	}
});
