/**
 * Copyright: Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
/**
 * Abstract base class for synchronous commands.
 */
Ext.define("seava.lib.e4e.js.dc.command.AbstractDcSyncCommand", {
	extend : "seava.lib.e4e.js.dc.command.AbstractDcCommand"

});