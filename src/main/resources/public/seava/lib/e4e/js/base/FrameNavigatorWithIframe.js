/**
 * Copyright: Nan21 Electronics SRL. All rights reserved. Use is subject to
 * license terms.
 */
seava.lib.e4e.js.base.FrameNavigatorWithIframe = {

	/**
	 * Maximum number of tabs (application frames) which are allowed to be
	 * opened at a certain moment. Use -1 for unlimited.
	 * 
	 * @type Integer
	 */
	maxOpenTabs : -1,

	/**
	 * When open a new frame, until the translation files are loaded, set the
	 * tab title as the frame fully qualified name or its simple name
	 */
	titleAsSimpleName : true,

	/**
	 * Lookup the application frame instance.
	 * 
	 * @param {}
	 *            frame
	 * @return {}
	 */
	getFrameInstance : function(frame) {
		var theIFrame =this.getIFrame(frame);
		var theFrameInstance = null;
		if (theIFrame) {
			theFrameInstance = theIFrame.theFrameInstance;
		}
		return theFrameInstance;
	},

	/**
	 * Lookup the IFrame.
	 * 
	 * @param {}
	 *            frame
	 * @return {}
	 */	
	getIFrame  : function(frame) {
		return window.frames[Constants.CMP_ID.FRAME_IFRAME_PREFIX
				+ frame];
	},
	
	/**
	 * Check if the given frame is already open.
	 * 
	 * @param {}
	 *            frame Frame name
	 * @return {}
	 */
	isFrameOpened : function(frameFqn) {
		return !Ext.isEmpty(document.getElementById(Main
				.getFrameIFrameId(frameFqn)));
	},

	/**
	 * Check if the given application frame is active, i.e. the corresponding
	 * tab panel is active.
	 * 
	 * @param {}
	 *            frame
	 * @return {}
	 */
	isFrameActive : function(frameFqn) {
		return (getApplication().getViewBody().getActiveTab().getId() == Main
				.getFrameTabId(frameFqn));
	},

	/**
	 * Show the given frame. If it is open activate it otherwise open a new tab
	 * and load it there.
	 * 
	 * @param {}
	 *            frame
	 * @param {}
	 *            params
	 */
	showFrame : function(frame, params) {
		this._showFrameImpl(frame, params);
	},

	/**
	 * Internal implementation function.
	 * 
	 * @param {}
	 *            frame
	 * @param {}
	 *            params
	 */
	_showFrameImpl : function(frameFqn, params) {

		if (!(params && params.url)) {
			alert("Programming error: params.url not specified in showFrame!");
			return;
		}

		var tabID = Main.getFrameTabId(frameFqn);
		var ifrID = Main.getFrameIFrameId(frameFqn);
		var vb = getApplication().getViewBody();

		var tabTtl = frameFqn;
		if (this.titleAsSimpleName) {
			tabTtl = tabTtl.substring(tabTtl.lastIndexOf('.') + 1,
					tabTtl.length);
		}

		if (Ext.isEmpty(document.getElementById(ifrID))
				&& !Ext.isEmpty(window.frames[ifrID])) {
			delete window.frames[ifrID];
		}

		if (this.isFrameOpened(frameFqn)) {
			if (!this.isFrameActive(frameFqn)) {
				vb.setActiveTab(tabID);
			}
		} else {
			if (this.maxOpenTabs > 0
					&& ((vb.items.getCount() + 1) == this.maxOpenTabs)) {
				Ext.Msg
						.alert(
								'Warning',
								'You have reached the maximum number of opened tabs ('
										+ (this.maxOpenTabs)
										+ ').<br> It is not allowed to open more tabs.');
				return;
			}

			var _odfn = function() {
				Ext.destroy(window.frames[this.n21_iframeID].__theViewport__);
				Ext.destroy(window.frames[this.n21_iframeID].theFrameInstance);
				try {
					delete window.frames[this.n21_iframeID];
				} catch (e) {
				}
				this.callParent();
			};

			var beforeCloseFn = function(tab, eOpts) {
				var _fr = window.frames[this.n21_iframeID];
				if (_fr && _fr.theFrameInstance
						&& _fr.theFrameInstance.isDirty()) {
					return confirm(Main.translate("msg",
							"dirty_data_on_frame_close"));
				}
				return true;
			};
			var onActivateFn = function(tab, eOpts) {
				var _fr = window.frames[this.n21_iframeID];
				if (_fr && _fr.theFrameInstance) {
					_fr.theFrameInstance.doLayout();
				}
			};

			var _p = new Ext.Panel(
					{
						onDestroy : _odfn,
						title : tabTtl,
						fqn : frameFqn,
						id : tabID,
						n21_iframeID : ifrID,
						autoScroll : true,
						layout : 'fit',
						closable : true,
						listeners : {
							beforeclose : {
								fn : beforeCloseFn
							},
							activate : {
								fn : onActivateFn
							}
						},
						html : '<div style="width:100%;height:100%;overflow: hidden;" id="div_'
								+ frameFqn
								+ '" ><iframe id="'
								+ ifrID
								+ '" name="'
								+ ifrID
								+ '" src="'
								+ params.url
								+ '" style="border:0;width:100%;height:100%;overflow: hidden" FRAMEBORDER="no"></iframe></div>'
					});
			vb.add(_p);
			vb.setActiveTab(tabID);
		}
	}
};
