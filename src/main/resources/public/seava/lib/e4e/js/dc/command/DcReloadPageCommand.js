/**
 * Copyright: Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.lib.e4e.js.dc.command.DcReloadPageCommand", {
	extend : "seava.lib.e4e.js.dc.command.AbstractDcAsyncCommand",

	dcApiMethod : seava.lib.e4e.js.dc.DcActionsFactory.RELOAD_PAGE,

	onExecute : function(options) {
		var dc = this.dc.store.reload();
	},

	isActionAllowed : function() {
		if (seava.lib.e4e.js.dc.DcActionsStateManager.isQueryDisabled(this.dc)) {
			this.dc.warning(Main.msg.DC_QUERY_NOT_ALLOWED, "msg");
			return false;
		}
		return true;
	}
});
